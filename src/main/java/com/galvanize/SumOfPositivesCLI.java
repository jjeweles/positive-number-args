package com.galvanize;

public class SumOfPositivesCLI {
    public static void main(String[] args) {
        try {
            int sum = 0;
            if (args.length < 1) {
                System.out.println(sum);
            } else {
                for (String arg : args) {
                    if (Integer.parseInt(arg) > 0) {
                        sum += Integer.parseInt(arg);
                    }
                }
                System.out.println("Sum of Arguments: " + sum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("\nProgram Ended.");
        }
    }
}